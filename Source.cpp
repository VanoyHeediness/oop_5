#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
#include <locale>
#include <Windows.h>
using namespace std;

struct  scan_info
{
	string model;                 //Наимиенование модели
	int price;                    //Цена
	double x_size;                //Горизонтальный размер области сканирования
	double y_size;                //Вертикальный размер области сканирования
	int optr;                     //Оптическое разрешение
	int grey;                     //Число градация серого
	void Input()
	{
		cout << "Введите модель: ";
		cin >> this->model;
		cout << "Цена: ";
		cin >> this->price;
		cout << "Горизонтальный размер области сканирования: ";
		cin >> this->x_size;
		cout << "Вертикальный размер области сканирования: ";
		cin >> this->y_size;
		cout << "Оптическое разрешение: ";
		cin >> this->optr;
		cout << "Число градация серого: ";
		cin >> this->grey;
	}
	//Так как у нас string, нужно реализовать вручную запись и чтение в файл
	void write(ofstream& of)
	{
		int len = this->model.length();
		of.write((char*)&len, sizeof(int));
		of.write((char*)model.data(), len);
		of.write((char*)&price, sizeof(int));
		of.write((char*)&x_size, sizeof(double));
		of.write((char*)&y_size, sizeof(double));
		of.write((char*)&optr, sizeof(int));
		of.write((char*)&grey, sizeof(int));
	}
	void read(ifstream& in)
	{
		int len = 100;
		in.read((char*)&len, sizeof(int));
		char* buf = new char[len];
		in.read(buf, len);
		this->model = "";
		for (int i = 0; i < len; i++)
			this->model += buf[i];
		in.read((char*)&price, sizeof(int));
		in.read((char*)&x_size, sizeof(double));
		in.read((char*)&y_size, sizeof(double));
		in.read((char*)&optr, sizeof(int));
		in.read((char*)&grey, sizeof(int));
		delete[] buf;
	}
	void Output()
	{
		cout << setw(15) << model << "|" << setw(6) << price << "|" << setw(7) << this->x_size << "|" << setw(7) << this->y_size;
		cout << "|" << setw(7) << optr << "|" << setw(7) << grey << "|\n";
	}
};

//@fname- имя и тип файла например out.bin
void WriteToBinary(const char* fname)
{
	ofstream out(fname, ios_base::binary | ios_base::out);
	int size = 7;	//Количество элементов
	scan_info* sinf = new scan_info[size];	//Выделение памяти под 7 элементов
	//Получение данных
	for (int i = 0; i < size; i++)
	{
		sinf[i].Input();
	}
	out.write((char*)&size, sizeof(int)); 
	for (int i = 0; i < size; i++)
	{
		sinf[i].write(out);
	}
	out.close();
}

//@ind -параметр что бы найти по индексу
//Возврашает 1 если элемент найден иначе -1
int ReadInBinary(int ind, const char* fname)
{
	ifstream inp(fname, ios_base::binary | ios_base::in);
	int size;
	inp.read((char*)&size, sizeof(int));
	scan_info ans;
	int ret = -1;
	for (int i = 0; i < size; i++)
	{
		ans.read(inp);
		if (i == ind)
		{
			ans.Output();
			ret = 1;
		}
	}
	inp.close();
	return ret;
}
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	WriteToBinary("Binary.bin");
	int ind;
	printf("Введите индекс: ");
	scanf("%i", &ind);
	int ans = ReadInBinary(ind, "Binary.bin");
	printf("Итого функция  прочтение: %i\n", ans);
	return 0;
}
